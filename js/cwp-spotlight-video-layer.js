(function($) {
  Drupal.behaviors.cwp_spotlight_video_layer = {
    attach: function (context, settings) {
      $('[data-fancybox]').once('cwp_spotlight_video_layer__fancybox').fancybox({
        youtube : {
          controls : 1,
          showinfo : 1,
          autoplay: 1,
        },
        // vimeo : {
        //   color : 'f00'
        // }
      });
    }
  };
})(jQuery);
