# Cyberwoven Paragraphs - Spotlight Video Layer

## CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers
  * TSD Snippet


## INTRODUCTION
---------------------

A Paragraph bundle made by Cyberwoven.


## REQUIREMENTS
---------------------

This module requires the following modules:

  * [Paragraphs](https://www.drupal.org/project/paragraphs)


## INSTALLATION
---------------------

  * Install the module as you normally would.
  * Verify installation by visiting /admin/structure/paragraphs_type and seeing
  your new Paragraph bundles.


## CONFIGURATION
---------------------

  * Go to your content type and add a new field of type Entity revisions,
    Paragraphs.
  * Allow unlimited so creators can add more that one Paragraph to the node.
  * On the field edit screen, you can add instructions, and choose which
    bundles you want to allow for this field.
  * Arrange them as you see fit.
  * Adjust your form display, placing the field where you want it.
  * Add the field into the Manage display tab.
  * Start creating content!

## THEMING
---------------------

There are styles in `scss` format to use as a base/jump-off-point in `scss/cwp-spotlight-video-layer.scss`.


## DEVELOPMENT/UPDATES
---------------------

### Styles:
  * `cd` into module directory and run `npm install`
  * Compile styles using: `npm run compile:styles`

### Development
**Tip: Use the script `clean-configs` to remove `UUID` and `config_hash` from exported configs.**

In your composer.json:

``` json
  {
    "repositories": [
      {
        "type": "package",
        "package": {
          "name": "cyberwoven/cwp-spotlight-video-layer",
          "version": "0.1.0",
          "type": "drupal-module",
          "dist": {
            "url": "https://bitbucket.org/cyberwoven/cwp-spotlight-video-layer/get/0.1.0.zip",
            "type": "zip"
          },
          "source": {
            "url": "git@bitbucket.org:cyberwoven/cwp-spotlight-video-layer.git",
            "type": "git",
            "reference": "0.1.0"
          }
        }
      },
    ],
    "require": {
      "cyberwoven/cwp-spotlight-video-layer": "0.1.0",
    }
  }
```

  * Make sure `version`, `reference` match the same tag/version in `require` *
  * The tag is the name of the zip folder. *
  * Pattern: `https://bitbucket.org/cyberwoven/<repo_name>/get/<tag_name>.zip` *


## MAINTAINERS
---------------------

Current maintainers:
  * [Tai Vu](https://www.drupal.org/u/taivu)

This project has been sponsored by:
  * [Cyberwoven](https://www.cyberwoven.com)

Inspired by:
  * [Bootstrap Paragraphs](https://www.drupal.org/project/bootstrap_paragraphs)


## TSD Snippet

``` md
### Spotlight Video Layer
  * Eyebrow Text (text field, optional)
  * Heading Text (text field, required)
  * Subheading Text (text field, optional)
  * Body (WYSIWYG, required)
  * Link (link field, optional)
  * Spotlight Image (image field, required)
  * Spotlight Video Url (embed video field, required)
  * Checkbox: Switch order of content/image (boolean field, optional)
```
